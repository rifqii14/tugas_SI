<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Teknik Mesin</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" >
  <link href="assets/ico/teknik.png" rel="icon">

  <meta name="description" content="" >
  <meta name="author" content="" >
  <!-- styles -->
  <link href="assets/css/bootstrap.css" rel="stylesheet" >
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet" >
  <link href="assets/css/prettyPhoto.css" rel="stylesheet" >
  <link href="assets/css/animate.css" rel="stylesheet" >
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic|Roboto+Condensed:400,300,700" rel="stylesheet" >
  <link href="assets/css/style.css" rel="stylesheet" >
  <link href="assets/color/default.css" rel="stylesheet" >
  <link href="assets/css/custom.css" rel="stylesheet">
</head>

<body>
  <header>
    <div id="topnav" class="navbar navbar-fixed-top default">
      <div class="navbar-inner">
        <div class="container">
          <div class="logo">
            <a href="index.html"><img class="logo" src="assets/img/logo-NAV.png" alt="" ></a>
          </div>
          <div class="navigation">
            <nav>
              <ul class="nav pull-right">
                <li><a href="index.php" class="external">Beranda</a></li>
                <li><a href="index.php#profil" class="external">Profil Fakultas</a></li>
                <li><a href="index.php#prodi" class="external">Prodi</a></li>
                <li><a href="index.php#dftdosen" class="external">Daftar Dosen</a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </header>
	<section id="beranda" class="inner">
		<div class="slogan">
		  <div class="icon">
		    <i class="icon-cog icon-10x"></i>
		  </div>
		  <h2><strong>TEKNIK MESIN</strong></h2>
		</div>
	</section>
	<!-- main -->
	<section id="maincontent" class="section">
		<div class="container">
			<div class="row">
				<div class="span12">
					<div class="row">
						<div class="span12">
							<div class="accordion" id="accordion2">
								<div class="accordion-group">
									<div id="collapseOne" class="accordion-body collapse in">
										<div class="accordion-inner">

											<p><strong>PROGRAM STUDI TEKNIK MESIN</strong></p>

											<p>Ketua Program Studi : Rifky, ST., MM.</p>

											<p>&nbsp;</p>

											<ol>
												<li><strong>VISI</strong></li>
											</ol>

											<p><em>Menjadi&nbsp;</em><em>p</em><em>rogram&nbsp;</em><em>s</em><em>tudi&nbsp;</em><em>utama&nbsp;</em><em>yang&nbsp;</em><em>menghasilkan lulusan teknik mesin yang&nbsp;&nbsp;</em><em>unggul&nbsp;</em><em>dalam kecerdasan</em>&nbsp;<em>spiritual</em><em>,</em>&nbsp;<em>intelektual</em><em>,</em><em>&nbsp;emosional dan&nbsp;</em><em>so</em><em>s</em><em>ial</em><em>&nbsp;dalam bidang manufaktur dan konversi energi pada tahun 2020.</em></p>

											<p>&nbsp;</p>

											<ol start="2">
												<li><strong>MISI</strong></li>
											</ol>

											<p>Lima pilar misi UHAMKA menjadi inspirasi rumusan misi FT UHAMKA yang dijadikan dasar perumusan Program Studi Teknik Mesin yang merupakan aspirasi dan komitmen bersama yaitu:</p>

											<ul>
												<li>Menjadikan Al-Islam dan Kemuhammadiyahan sebagai ruh dan bagian dari pendidikan, pengajaran, bersikap dan berprilaku segenap civitas akademika Program Studi Teknik Mesin.</li>
												<li>Menyelenggarakan pendidikan dan pengajaran yang bermutu</li>
												<li>Menyelenggarakan penelitian bidang menufaktur dan konversi energi yang berkontribusi pada kemajuan ilmu pengetahuan dan teknologi dalam skala nasional</li>
												<li>Menyelenggarakan pengabdian dan pemberdayaan masyarakat yang berkontribusi dalam memecahkan masalah masyarakat dan peningkatan mutu pendidikan manufaktur dan konversi energi.</li>
												<li>Mengembangkan jiwa kewirausahaan dalam bidang manufaktur dan konversi energi.</li>
												<li>Membangun sistem pengelolaan&nbsp; program studi yang kuat, amanah, demokratis</li>
											</ul>

											<p>&nbsp;</p>

											<p><strong>3. TUJUAN</strong></p>

											<ul>
												<li>Mewujudkan Program Studi sebagai pusat unggulan gerakan dakwah Muhammadiyah yang menghasilkan kader persyarikatan, kader umat dan kader bangsa yang berakhlak mulia, untuk menjadi pilar dalam masyarakat utama yang diridhai oleh Allah Swt.</li>
												<li>Menghasilkan lulusan teknik mesin yang sukses berkarier dan menjadi profesional yang produktif.</li>
												<li>Menghasilkan penelitian bidang manufaktur dan konversi energi berskala nasional dan internasional yang bermanfaat bagi pengembangan ilmu, dunia usaha, dan masyarakat luas.</li>
												<li>Mewujudkan pengabdian dan pemberdayaan masyarakat bidang manufaktur dan konversi energi yang bermanfaat bagi persyarikatan Muhammadiyah dan masyarakat.</li>
												<li>Menghasilkan sumber daya manusia yang percaya pada diri sendiri, berjiwa wirausaha, serta beramal sesuai bidang ilmu teknik mesin untuk mewujudkan masyarakat Islam yang berkemajuan.</li>
												<li>Menghasilkan sumber daya insani yang berkarakter, cerdas, kreatif, dan kompetitif dalam&nbsp; skala nasional dan internasional.</li>
											</ul>

											<p>&nbsp;</p>

											<ol start="4">
												<li><strong><strong>SASARAN</strong></strong>Sejalan dengan visi dan misi serta tujuan Program Studi Teknik Mesin, maka &nbsp;ditetapkan beberapa sasaran yang mencakup bidang:

												<ol>
													<li><strong>Al-Islam dan Kemuhammadiyahan</strong></li>
												</ol>

												<p>Peningkatan pengetahuan dan kemampuan dalam mengimplementasikan</p>

												<p>Gerakan Muhammadiyah Berkemajuan</p>

												<ol start="2">
													<li><strong>Pendidikan dan Pengajaran</strong></li>
													<li>Pemutakhiran Kurikulum</li>
													<li>Peningkatan Kualitas Dosen</li>
													<li>Peningkatan Kualitas Proses Pembelajaran</li>
													<li>Peningkatan Suasana Akademik</li>
													<li>Peningkatan Kompetensi Mahasiswa</li>
													<li>Peningkatan Kualitas Lulusan</li>
													<li><strong>Penelitian dan Pengembangan</strong></li>
													<li>Penyelenggaraan penelitian teknik mesin</li>
													<li>Peningkatan publikasi karya ilmiah di bidang teknik mesin</li>
													<li><strong>Bidang Pemberdayaan dan Pengabdian Masyarakat</strong></li>
												</ol>

												<p>Penyelenggaraan Pemberdayaan dan Pengabdian Masyarakat mengacu bidang teknik mesin yang berdampak pada peningkatan:</p>

												<ol>
													<li>Produktivitas</li>
													<li>Kesejahteraan Masyarakat</li>
													<li>Mutu Lingkungan</li>
												</ol>

												<p>&nbsp;</p>

												<ol>
													<li><strong>Strategi Pencapaian Sasaran</strong></li>
												</ol>

												<p>Strategi Pencapaian Sasaran&nbsp; adalah langkah-langkah sistematis yang dikembangkan dalam rangka untuk mencapai sasaran. Berikut strategi dan sasaran yang lebih rinci sampaikan melalui matriks di bawah ini.</p>
												</li>
											</ol>

											<p>&nbsp;</p>

											<ol start="5">
												<li><strong>KOMPETENSI LULUSAN</strong></li>
											</ol>

											<ul>
												<li><strong>KompetensiUtama</strong></li>
											</ul>

											<ol>
												<li>Memiliki keahlian dasar dalam bidangTeknik Mesin</li>
												<li>Mampu mengembangkan diri dan ilmunya melalui kegiatan penelitian.</li>
												<li>Mampu menganalisis dan memecahkan setiap permasalahan secara ilmiah.</li>
												<li>Mampu merekayasa produk mesin sesuai dengan kebutuhan pengguna</li>
												<li>Mampu mengoptimalisasi produk mesin</li>
											</ol>

											<ul>
												<li><strong>Kompetensi Pendukung</strong></li>
											</ul>

											<ol>
												<li>Memiliki kemampuan komunikasi bahasa Inggris</li>
												<li>Dapat menggunakan sarana praktik/praktikum di Laboratorium dengan baik.</li>
												<li>Dapat melakukan penelitian dengan sarana yang ada</li>
												<li>Dapat merancang, merencanakan mesin serta peralatan mesin</li>
												<li>Dapat mengelola instalasi produksi</li>
											</ol>

											<ul>
												<li><strong>Kompetensi Lainnya (<em>Soft Skill</em>)</strong>

												<ol>
													<li>Beriman dan bertaqwa kepada Tuhan YME, berakhlak mulia, memiliki etika dan moral kebangsaan, berkepribadian luhur dan mandiri serta bertanggungjawab terhadap masyarakat dan bangsa.</li>
													<li>Mampu terlibat dalam kehidupan social bermasyarakat berdasarkan Pancasila.</li>
													<li>Memiliki jiwa kepemimpinan, peneliti, dan&nbsp;<em>entrepreneurship&nbsp;</em>serta mampu bersaing di era global.</li>
												</ol>
												</li>
											</ul>

											<p>&nbsp;</p>

											<table border="1" cellpadding="10px">
												<tbody>
													<tr>
														<th>No.</th>
														<th>PROFIL</th>
														<th>DESKRIPSI PROFIL</th>
													</tr>
													<tr>
														<td>1.</td>
														<td>PerancangSistemTermal</td>
														<td>Pembuatrancanganelemenmesinmenjadisebuahsistemtermal</td>
													</tr>
													<tr>
														<td>2.</td>
														<td>PerancangSistemMekanis</td>
														<td>Pembuatrancanganelemenmesinmenjadi
														<p>&nbsp;</p>

														<p>sebuahsistemmekanikal</p>
														</td>
													</tr>
													<tr>
														<td>3.</td>
														<td>Supervisor Produksi
														<p>&nbsp;</p>

														<p>dan<em>Maintenance</em></p>
														</td>
														<td>Pelakupengawasanpekerjaanproduksi
														<p>&nbsp;</p>

														<p>Pengawaspadapekerjaanpemeliharaandan</p>

														<p>perawatanmesin.</p>
														</td>
													</tr>
													<tr>
														<td>4.</td>
														<td>PengawasMutuProduk&nbsp; (<em>QC</em>)</td>
														<td>Pelakupengawasanmutuproduk</td>
													</tr>
													<tr>
														<td>5.</td>
														<td>PengelolaProyekMekanik</td>
														<td>Pengelolaproyekmekanikaldi bidangindustri
														<p>&nbsp;</p>

														<p>dankonstruksi</p>
														</td>
													</tr>
													<tr>
														<td>6.</td>
														<td>Pelaksana<em>R &amp; D</em></td>
														<td>Penelitidalampengembanganproduk</td>
													</tr>
													<tr>
														<td>7.</td>
														<td>Ilmuwanteknikmesin</td>
														<td>Pelaku yang ahlidanmengembangkan
														<p>&nbsp;</p>

														<p>ilmuteknikmesin</p>
														</td>
													</tr>
													<tr>
														<td>8.</td>
														<td><em>Entrepreneur</em></td>
														<td>Pelakuwirausahaberbasis
														<p>&nbsp;</p>

														<p>teknikmesin</p>
														</td>
													</tr>
													<tr>
														<td>9.</td>
														<td><em>Sales Engineer</em></td>
														<td>Pelakupenjualprodukteknikmesin</td>
													</tr>
												</tbody>
											</table>

											<ol start="6">
												<li><strong>PROFIL LULUSAN</strong></li>
											</ol>

											<p><strong>&nbsp;</strong></p>

											<p><strong>KURIKULUM PROGRAM STUDI TEKNIK MESIN</strong></p>

											<p>Kurikulum Program Studi Teknik MesinProgram Sarjana(S1) Fakultas Teknik Universitas Muhammadiyah Prof. DR. HAMKA dikelompokkan menurut jenis mata kuliah.</p>

											<!-- taro sini -->
											<p><b>DAFTAR DOSEN<b></p>
												<ol>
													<li> Agus Fikri, ST. MM. </li>
													<li> Rifky, ST., MM.</li>
													<li> Pancatatva Hesti Gunawan, ST., MT.</li>
												</ol>
										</div>
									</div>
								</div>
							</div>
							<!--end detail -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer>
	  <div class="verybottom">
	    <div class="container">
	      <div class="row">
	        <div class="span12">
	            <p>
	              Dibuat oleh kelompok 2 , kelas 2A :
	             
	            </p>
	          <div class="credits">
	              1. Muhammad Rifqi Maulatur Rahman (1703015041)<br>
	              2. Muhammad Khoiri Muzakki (1703015123)<br>
	              3. Rinaldi Hari Hermawan (1703015084)<br>
	              4. Muhammad Sabiil (1703015163) <br>
	              5. Nur Rahmad (1703015185)<br>
	              6. Yudha Adi H.P (1703015030)<br>
	              7. Monica Dwijayanti (1703015112)<br>
	              8. Azzizah (1703015188)
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</footer>
	<!-- end main -->
	<!-- js -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/jquery.easing.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/parallax/jquery.parallax-1.1.3.js"></script>
	<script src="assets/js/nagging-menu.js"></script>
	<script src="assets/js/jquery.nav.js"></script>
	<script src="assets/js/prettyPhoto/jquery.prettyPhoto.js"></script>
	<script src="assets/js/portfolio/jquery.quicksand.js"></script>
	<script src="assets/js/portfolio/setting.js"></script>
	<script src="assets/js/hover/jquery-hover-effect.js"></script>
	<script src="assets/js/jquery.scrollTo.min.js"></script>
	<script src="assets/js/animate.js"></script>


	<script src="assets/js/custom.js"></script>

</body>

</html>
