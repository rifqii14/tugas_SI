<?php 
  include("aksi/koneksi.php")
?>
<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Detail Dosen</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" >
  <link href="assets/ico/teknik.png" rel="icon">

  <meta name="description" content="" >
  <meta name="author" content="" >
  <!-- styles -->
  <link href="assets/css/bootstrap.css" rel="stylesheet" >
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet" >
  <link href="assets/css/prettyPhoto.css" rel="stylesheet" >
  <link href="assets/css/animate.css" rel="stylesheet" >
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic|Roboto+Condensed:400,300,700" rel="stylesheet" >
  <link href="assets/css/style.css" rel="stylesheet" >
  <link href="assets/color/default.css" rel="stylesheet" >
  <link href="assets/css/custom.css" rel="stylesheet">
</head>

<body>
  <header>
    <div id="topnav" class="navbar navbar-fixed-top default">
      <div class="navbar-inner">
        <div class="container">
          <div class="logo">
            <a href="index.html"><img class="logo" src="assets/img/logo-NAV.png" alt="" ></a>
          </div>
          <div class="navigation">
            <nav>
              <ul class="nav pull-right">
                <li><a href="index.php" class="external">Beranda</a></li>
                <li><a href="index.php#profil" class="external">Profil Fakultas</a></li>
                <li><a href="index.php#prodi" class="external">Prodi</a></li>
                <li><a href="index.php#dftdosen" class="external">Daftar Dosen</a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </header>
  <?php 
  	if(isset($_GET['id'])){
  		$id = $_GET['id'];
		$res = mysqli_query($conn,"SELECT * FROM tbl_dosen WHERE nidn = '$id' ");
		while ($row = mysqli_fetch_assoc($res)) {

  ?>
	<section id="beranda" class="inner">
		<div class="container">
			<div class="row">
				<div class="span12">
					<div class="inner-heading">
						<!-- bikin else if lanjutin ampe Z ya -->
						
						<?php

						if(substr($row["nama"],0,1) == 'A' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/A.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'B' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/B.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'C' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/C.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'D' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/D.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'E' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/E.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'F' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/F.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'G' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/G.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'H' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/H.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'I' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/I.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'J' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/J.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'K' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/K.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'L' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/L.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'M' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/M.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'N' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/N.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'O' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/O.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'P' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/P.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'Q' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/Q.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'R' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/R.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'S' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/S.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'T' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/T.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'U' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/U.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'V' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/V.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'W' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/W.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'X' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/X.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'Y' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/Y.jpeg'>";
						}
						else if(substr($row["nama"],0,1) == 'Z' && $row["photo"] == NULL) {
							echo "<img class='propil' src='assets/img/dummy_photo/Z.jpeg'>";
						}else{
		
				
						?>
						<img class="propil" src="data:image/jpeg;base64, <?=base64_encode( $row["photo"]) ?>">

						<?php } ?>

						<h2><strong><?= $row['nama']?></strong></h2>

					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- main -->
	<section id="maincontent" class="section">
		<div class="container">
			<div class="row">
				<div class="span12">
					<div class="row">
						<div class="span12">
							<center><h4><strong>Detail</strong></h4></center>
							<!-- end detail -->
							<div class="accordion" id="accordion2">
								<div class="accordion-group">
									<div id="collapseOne" class="accordion-body collapse in">
										<div class="accordion-inner">
											<center>
											<table>
												<tr>
													<td style="width:45%"><strong>NID</strong></td>
													<td style="width:5%"><strong>:</strong></td>
													<td><?= $row['nidn'] ?></td>
												</tr>
												<tr>
													<td><strong>Nama</strong></td>
													<td><strong>:</strong></td>
													<td><?= $row['nama'] ?></td>
												</tr>
												<tr>
													<td><strong>Alamat</strong></td>
													<td><strong>:</strong></td>
													<td><?= $row['alamat'] ?></td>
												</tr>
												<tr>
													<td><strong>Nomor telepon</strong></td>
													<td><strong>:</strong></td>
													<td><?= $row['telepon'] ?></td>
												</tr>
											</table>

											<br><strong>Jadwal Semester Genap 2017/2018</strong>
											<br><br>
					<img src="data:image/jpeg;base64, <?=base64_encode( $row["jadwal"]) ?>">
										</center>
										</div>
									</div>
								</div>
							</div>
							<!--end detail -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end main -->
	<?php
		}
	}
	?>
	<footer>
	  <div class="verybottom">
	    <div class="container">
	      <div class="row">
	        <div class="span12">
	            <p>
	              Dibuat oleh kelompok 2 , kelas 2A :
	            </p>
	          <div class="credits">
	              1. Muhammad Rifqi Maulatur Rahman (1703015041)<br>
	              2. Muhammad Khoiri Muzakki (1703015123)<br>
	              3. Rinaldi Hari Hermawan (1703015084)<br>
	              4. Muhammad Sabiil (1703015163) <br>
	              5. Nur Rahmad (1703015185)<br>
	              6. Yudha Adi H.P (1703015030)<br>
	              7. Monica Dwijayanti (1703015112)<br>
	              8. Azzizah (1703015188)
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</footer>
	<!-- js -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/jquery.easing.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/parallax/jquery.parallax-1.1.3.js"></script>
	<script src="assets/js/nagging-menu.js"></script>
	<script src="assets/js/jquery.nav.js"></script>
	<script src="assets/js/prettyPhoto/jquery.prettyPhoto.js"></script>
	<script src="assets/js/portfolio/jquery.quicksand.js"></script>
	<script src="assets/js/portfolio/setting.js"></script>
	<script src="assets/js/hover/jquery-hover-effect.js"></script>
	<script src="assets/js/jquery.scrollTo.min.js"></script>
	<script src="assets/js/animate.js"></script>


	<script src="assets/js/custom.js"></script>

</body>

</html>
