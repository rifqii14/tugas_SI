<?php 
  include("aksi/koneksi.php")
?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">

  <title>Fakultas Teknik UHAMKA</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" >
  <link href="assets/ico/teknik.png" rel="icon">
  <meta name="description" content="" >
  <meta name="author" content="" >
  <!-- styles -->
  <link href="assets/css/bootstrap.css" rel="stylesheet" >
  <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.16/css/jquery.dataTables.css">
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet" >
  <link href="assets/css/prettyPhoto.css" rel="stylesheet" >
  <link href="assets/css/animate.css" rel="stylesheet" >
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic|Roboto+Condensed:400,300,700" rel="stylesheet" >
  <link href="assets/css/style.css" rel="stylesheet" >
  <link href="assets/color/default.css" rel="stylesheet" >
  <link href="assets/css/custom.css" rel="stylesheet">

</head>

<body>
  <header>
    <div id="topnav" class="navbar navbar-fixed-top default">
      <div class="navbar-inner">
        <div class="container">
          <div class="logo">
            <a href="index.html"><img class="logo" src="assets/img/logo-NAV.png" alt="" ></a>
          </div>
          <div class="navigation">
            <nav>
              <ul class="nav pull-right">
                <li class="current"><a href="#beranda">Beranda</a></li>
                <li><a href="#profil">Profil Fakultas</a></li>
                <li><a href="#prodi">Prodi</a></li>
                <li><a href="#dftdosen">Daftar Dosen</a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- intro -->
  <section id="beranda">
    <div class="slogan">
      <img src="assets/img/logo-slogan.png" width="200" height="60">
      <h1>FAKULTAS TEKNIK UHAMKA</h1>
    </div>
  </section>
  <!-- end intro -->

  <!-- profil -->
  <section id="profil" class="section">
    <div class="container">
      <div class="row">
        <div class="span12">
          <div class="heading">
            <h3><span>Profil Fakultas</span></h3>
          </div>
          <div class="sub-heading">
            <p>
Fakultas Teknik (FT) Universitas Muhammadiyah Prof.DR. HAMKA (UHAMKA) didirikan pada tanggal 30 Mei 1997 sesuai SK Direktorat Jenderal Pendidikan Tinggi Departemen Pendidikan dan Kebudayaan Republik Indonesia Nomor 138/DIKTI/Kep/1997. Saat ini Fakultas Teknik UHAMKA mengelola tiga Program Studi yang ketiganya telah terakreditasi yaitu:

Informatika, jenjang Strata Satu (S-1)
Teknik Elektro, jenjang Strata Satu (S-1)
Teknik Mesin, jenjang Strata Satu (S-1)
Saat ini telah meluluskan mahasiswa dengan rata-rata masa studi 4,5 tahun dan telah bekerja pada bidangnya dengan masa tunggu rata-rata kurang dari 6 bulan. Fakultas teknik UHAMKA berpartisipasi aktif dalam kegiatan Kontes Robot Indonesia sejak tahun 2005 sampai sekarang. Para dosen dan mahasiswa berperan aktif dalam kegiatan forum diskusi  ilmiah.
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="span4">
          <img src="assets/img/profil/prof1.jpg" alt="" class="img-polaroid" >
        </div>
        <div class="span4">
          <img src="assets/img/profil/prof2.jpg" alt="" class="img-polaroid" >
        </div>
        <div class="span4">
          <img src="assets/img/profil/prof3.jpg" alt="" class="img-polaroid" >
        </div>
      </div>
    </div>
  </section>
  <!-- end profil -->

  <!-- prodi -->
  <section id="prodi" class="section parallax dark">
    <div class="container">
      <div class="row">
        <div class="span12">
          <div class="heading">
            <h3><span>Program Studi</span></h3>
          </div>
          <div class="sub-heading">
            <p>
              Fakultas Teknik UHAMKA Mempunyai 3 Program Studi
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="span4">
          <div class="box aligncenter">
        <a href="informatika.php">
            <div class="halftop">
              <h4><strong>Teknik Informatika</strong></h4>
              <i class="icon-desktop icon-4x"></i>
            </div>
        </a>
            <p>
              Ketua Program Studi :  <br> Arry Avorizano, S.Kom.,M.Kom
            </p>
          </div>
        </div>
      
        <div class="span4">
          <div class="box aligncenter">
        <a href="elektro.php">
            <div class="halftop">
              <h4><strong>Teknik Elektro</strong></h4>
              <i class="icon-bolt icon-4x"></i>
            </div>
        </a>
            <p>
              Ketua Program Studi : <br> Oktarina Heriyani, S.Si., MT
            </p>
          </div>
        </div>
        <div class="span4">
          <div class="box aligncenter">
        <a href="mesin.php">
            <div class="halftop">
              <h4><strong>Teknik Mesin</strong></h4>
              <i class="icon-cog icon-4x"></i>
            </div>
        </a>
            <p>
              Ketua Program Studi : <br> Rifky, ST., MM
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end prodi -->

  <!-- section dosen -->
  <section id="dftdosen" class="section">
    <div class="container">
      <div class="row">
        <div class="span12">
          <div class="heading">
            <h3><span>DAFTAR DOSEN</span></h3>
          </div>
          <div class="sub-heading">
            <P>Daftar Dosen Fakultas TEKNIK UHAMKA</P>
          </div>
            <table id="dosen" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                    <th>No.</th>
                    <th>NID</th>
                    <th>Nama Dosen</th>
                    <th>Program Studi</th>
                    <th>Detail</th>
                  </tr>
              </thead>
              <tbody>
                <?php
                $i = 1;
                $res = mysqli_query($conn,"SELECT * FROM tbl_dosen");
                  while($row = mysqli_fetch_assoc($res)){
                    echo                
                    "<tr>
                        <td style='width:5%'><center>".$i++."</center></td>
                        <td>".$row['nidn']."</td>
                        <td>".$row['nama']."</td>
                        <td>".$row['prodi']."</td>
                        <td><center><a href='detail_dosen.php?id=".$row['nidn']."'><i class='icon icon-eye-open'></i></a></center></td>
                    </tr>";
                  }
                ?>
               </tbody>
            </table>
        </div>
      </div>
    </div>
  </section>
  <!-- end dosen -->

  <footer>
    <div class="verybottom">
      <div class="container">
        <div class="row">
          <div class="span12">
              <p>
                Dibuat oleh kelompok 2 , kelas 2A :
               
              </p>
            <div class="credits">
                1. Muhammad Rifqi Maulatur Rahman (1703015041)<br>
                2. Muhammad Khoiri Muzakki (1703015123)<br>
                3. Rinaldi Hari Hermawan (1703015084)<br>
                4. Muhammad Sabiil (1703015163) <br>
                5. Nur Rahmad (1703015185)<br>
                6. Yudha Adi H.P (1703015030)<br>
                7. Monica Dwijayanti (1703015112)<br>
                8. Azzizah (1703015188)
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>


  <!-- js-->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/datatables/datatables.js"></script>
  <script src="assets/js/dataTables.bootstrap.min.js"></script>
  <script src="assets/js/jquery.easing.js"></script>
  <script src="assets/js/parallax/jquery.parallax-1.1.3.js"></script>
  <script src="assets/js/nagging-menu.js"></script>
  <script src="assets/js/jquery.nav.js"></script>
  <script src="assets/js/prettyPhoto/jquery.prettyPhoto.js"></script>
  <script src="assets/js/portfolio/jquery.quicksand.js"></script>
  <script src="assets/js/portfolio/setting.js"></script>
  <script src="assets/js/hover/jquery-hover-effect.js"></script>
  <script src="assets/js/jquery.scrollTo.min.js"></script>
  <script src="assets/js/animate.js"></script>
  <script src="assets/js/custom.js"></script>
  <script>
    $(document).ready(function() {
    $('#dosen').DataTable({

    });
} );
  </script>
</body>
</html>
