<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Teknik Informatika</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" >
  <link href="assets/ico/teknik.png" rel="icon">

  <meta name="description" content="" >
  <meta name="author" content="" >
  <!-- styles -->
  <link href="assets/css/bootstrap.css" rel="stylesheet" >
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet" >
  <link href="assets/css/prettyPhoto.css" rel="stylesheet" >
  <link href="assets/css/animate.css" rel="stylesheet" >
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic|Roboto+Condensed:400,300,700" rel="stylesheet" >
  <link href="assets/css/style.css" rel="stylesheet" >
  <link href="assets/color/default.css" rel="stylesheet" >
  <link href="assets/css/custom.css" rel="stylesheet">
</head>

<body>
  <header>
    <div id="topnav" class="navbar navbar-fixed-top default">
      <div class="navbar-inner">
        <div class="container">
          <div class="logo">
            <a href="index.html"><img class="logo" src="assets/img/logo-NAV.png" alt="" ></a>
          </div>
          <div class="navigation">
            <nav>
              <ul class="nav pull-right">
                <li><a href="index.php" class="external">Beranda</a></li>
                <li><a href="index.php#profil" class="external">Profil Fakultas</a></li>
                <li><a href="index.php#prodi" class="external">Prodi</a></li>
                <li><a href="index.php#dftdosen" class="external">Daftar Dosen</a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </header>
	<section id="beranda" class="inner">
		<div class="slogan">
		  <div class="icon">
		    <i class="icon-desktop icon-10x"></i>
		  </div>
		  <h2><strong>TEKNIK INFORMATIKA</strong></h2>
		</div>
	</section>
	<!-- main -->
	<section id="maincontent" class="section">
		<div class="container">
			<div class="row">
				<div class="span12">
					<div class="row">
						<div class="span12">
							<div class="accordion" id="accordion2">
								<div class="accordion-group">
									<div id="collapseOne" class="accordion-body collapse in">
										<div class="accordion-inner">
				<p><strong>PROGRAM STUDI INFORMATIKA</strong></p>

				<p>Ketua Program Studi : &nbsp;<em>Arry Avorizano, S.Kom.,M.Kom</em>.</p>

				<p>&nbsp;</p>

				<p><strong>VISI</strong></p>

				<p><em>Menjadi&nbsp;</em><em>p</em><em>rogram&nbsp;</em><em>s</em><em>tudi&nbsp;</em><em>utama&nbsp;</em><em>yang&nbsp;</em><em>menghasilkan lulusan teknik i</em><em>nformatika&nbsp;</em><em>yang&nbsp;</em><em>unggul&nbsp;</em><em>dalam kecerdasan s</em><em>piritual, intelektual,&nbsp;</em><em>emosional dan sosial pada</em>&nbsp;<em>bidang</em><em>&nbsp;jaringan komputer</em><em>, multimedia, rekayasa perangkat lunak</em>&nbsp;<em>pada tahun 2020</em><em>.</em></p>

				<p><strong>MISI</strong></p>

				<ol>
					<li>Menjadikan Al-Islam dan Kemuhammadiyahan sebagai ruh dan bagian dari pendidikan, pengajaran, bersikap dan berprilaku pada segenap civitas akademika di Program Studi Teknik Informatika</li>
					<li>Menyelenggarakan pendidikan dan pengajaran dibidang teknik informatika yang bermutu</li>
					<li>Menyelenggarakan penelitian di bidang teknik informatika yang berkontribusi pada kemajuan ilmu pengetahuan dan teknologi dalam skala nasional dan internasional</li>
					<li>Menyelenggarakan pengabdian dan pemberdayaan masyarakat yang berkontribusi dalam memecahkan masalah masyarakat dan peningkatan mutu pendidikan teknik informatika.</li>
					<li>Mengembangkan jiwa kewirausahaan dalam bidang teknik informatika</li>
					<li>Membangun sistem pengelolaan&nbsp; di Program Studi Teknik Informatika &nbsp;yang kuat, amanah dan demokratis</li>
				</ol>

				<p>&nbsp;</p>

				<p><strong>TUJUAN</strong></p>

				<ol>
					<li>Mewujudkan Program Studi Teknik Informatika di FT-UHAMKA sebagai bagian unggulan gerakan dakwah Muhammadiyah yang menghasilkan kader persyarikatan, kader umat dan kader bangsa yang berakhlak mulia, untuk menjadi pilar dalam masyarakat utama yang diridhai oleh Allah Swt.</li>
					<li>Menghasilkan lulusan teknik informatika yang sukses berkarier dan menjadi profesional yang produktif.</li>
					<li>Menghasilkan penelitian dibidang teknik informatika berskala nasional dan internasional yang bermanfaat bagi pengembangan ilmu, dunia usaha, dan masyarakat luas.</li>
					<li>Mewujudkan pengabdian dan pemberdayaan masyarakat dibidang teknik informatika yang bermanfaat bagi persyarikatan Muhammadiyah dan masyarakat.</li>
					<li>Menghasilkan sumber daya manusia yang percaya pada diri sendiri, berjiwa wirausaha, serta beramal sesuai bidang ilmu untuk mewujudkan masyarakat Islam yang berkemajuan.</li>
					<li>Memiliki sistem pengelolaan program studi yang kuat, amanah dan demokratis.</li>
				</ol>

				<p>&nbsp;</p>

				<p><strong>SASARAN</strong></p>

				<p>Sasaran yang ingin dicapai oleh Program Studi Teknik Informatika pada tahun akademik 2018/2019 dengan strategi pencapaian dikelompokan kedalam program/kegiatan yang mencakup:</p>

				<ol>
					<li><strong>Pendidikan dan pengajaran</strong></li>
				</ol>

				<p>Sasaran yang ingin dicapai dari program/kegiatan pendidikan dan pengajaran pada Program Studi Teknik Informatika yaitu:</p>

				<ul>
					<li>Terjadi peningkatan nilai rata-rata Indeks Prestasi Kumulatif (IPK) lulusan Program Studi Teknik Informatika FT-UHAMKA &ge; 3,30;</li>
					<li>Tingkat kelulusan mahasiswa yang mampu menyelesaikan studi tepat waktu (8 semester) mencapai jumlah di atas 60 %;</li>
					<li>Waktu tunggu lulusan untuk memperoleh pekerjaan yang sesuai dengan bidangnya maksimal rata-rata di bawah 5 bulan;</li>
					<li>Tersedianya dokumen kurikulum sesuai visi dan misi Program Studi Teknik Informatika dan perkembangan lingkungan internal dan eksternal.</li>
				</ul>

				<p>&nbsp;</p>

				<ol start="2">
					<li><strong>Penelitian dan Publikasi Ilmiah</strong></li>
				</ol>

				<p>Sasaran yang ingin dicapai dari program/kegiatan penelitian dan publikasi ilmiah adalah:</p>

				<ul>
					<li>Terealisasinya secara bertahap jumlah penelitian sebanyak jumlah dosen tetap pada Program Studi Teknik Informatika masing-masing 2 (dua) untuk setiap tahun;</li>
					<li>Diperolehnya dana penelitian yang dibiayai oleh pihak eksternal seperti: Kemenristek Dikti, Pemerintah Daerah, dan CSR;</li>
					<li>Meningkatnya keterlibatan mahasiswa dalam penelitian yang dilakukan oleh dosen-dosen Program Studi Teknik Informatika;</li>
					<li>Terpublikasinya hasil penelitian dalam Jurnal Rekayasa Teknologi FT-UHAMKA dan secara bertahap terpublikasi dalam jurnal terakreditasi serta terindeks;</li>
					<li>Terwujudnya jurnal&nbsp;<em>online</em>&nbsp;Program Studi Teknik Informatika FT-UHAMKA.</li>
				</ul>

				<p>&nbsp;</p>

				<ol start="3">
					<li><strong>Pengabdian kepada Masyarakat</strong></li>
				</ol>

				<p>Sasaran yang ingin dicapai dari program/kegiatan pengabdian kepada masyarakat adalah</p>

				<ul>
					<li>Tercapainya secara bertahap jumlah pengabdian kepada masyarakat sebanyak jumlah dosen tetap pada Program Studi Teknik Informatika masing-masing 2 (dua) untuk setiap tahun.</li>
					<li>Meningkatnya jumlah pengabdian kepada masyarakat yang dibiayai oleh pihak eksternal seperti: Kemenristekdikti, pemerintah daerah, dan CSR.</li>
					<li>Meningkatnya keterlibatan mahasiswa dalam pengabdian kepada masyarakat yang dilakukan oleh dosen Program Studi Teknik Informatika.</li>
				</ul>

				<p>&nbsp;</p>

				<ol start="4">
					<li><strong>Pembinaan Kemahasiswaan</strong></li>
				</ol>

				<p>Sasaran yang ingin dicapai dari program/kegiatan pembinaan kemahasiswaan pada Program Studi Teknik Informatika yaitu:</p>

				<ul>
					<li>Menjadikan mahasiswa berakhlakul karimah dengan ajaran Islam sesuai tuntunan Alquran dan hadist</li>
					<li>Membentuk mahasiswa berprestasi hingga mendapatkan penghargaan tingkat nasional dan internasional di bidang seni, budaya dan olahraga;</li>
					<li>Meningkatnya nilai rata-rata kemampuan mahasiswa dalam berbahasa Inggris;</li>
					<li>Menarik keterlibatan mahasiswa dalam program kreativitas mahasiswa (PKM).</li>
				</ul>

				<p>&nbsp;</p>

				<ol start="5">
					<li><strong>Sumber Daya Manusia</strong></li>
				</ol>

				<p>Sasaran yang ingin dicapai dari program/kegiatan sumber daya manusia adalah:</p>

				<ul>
					<li>Tersedianya dosen sesuai dengan kualifikasi dan kompetensi yang dibutuhkan Program Studi Teknik Informatika UHAMKA;</li>
					<li>Meningkatnya jumlah jenjang kepangkatan akademik dosen dan jumlah dosen yang tersertifikasi (serdos);</li>
					<li>Meningkatnya jumlah tenaga kependidikan berkualifikasi sarjana sesuai dengan bidangnya.</li>
				</ul>

				<p>&nbsp;</p>

				<ol start="6">
					<li><strong>Kelembagaan dan Tata Laksana</strong></li>
				</ol>

				<p>Sasaran yang ingin dicapai dari program/kegiatan kelembagaan dan tata laksana adalah:</p>

				<ul>
					<li>Terselenggaranya pelayanan administrasi kesekretariatan yang ramah, cepat, tertib dan sopan.</li>
					<li>Terselenggaranya Sistem Informasi Akademik yang didukung oleh kesiapan sumber daya manusia dan teknologi di lingkungan fakultas Teknik-UHAMKA.</li>
				</ul>

				<p>&nbsp;</p>

				<ol start="7">
					<li><strong>Keuangan dan Sarana Prasarana</strong></li>
				</ol>

				<p>Sasaran yang ingin dicapai dari program/kegiatan keuangan dan sarana prasarana adalah:</p>

				<ul>
					<li>Tersedianya ruang perkuliahan yang mampu meningkatkan daya tampung mahasiswa baru untuk memberi akses yang lebih luas kepada masyarakat dalam mencapai jenjang pendidikan tinggi;</li>
					<li>Tersedianya prasarana perkuliahan berbasis teknologi informasi dan komunikasi;</li>
					<li>Terpenuhinya kecukupan fasilitas perpustakaan, sarana laboratorium, ruang dosen, dan ruang aula fakultas yang representatif.</li>
				</ul>

				<p>&nbsp;</p>

				<ol start="8">
					<li><strong>Kewirausahaan dan kerjasama</strong></li>
				</ol>

				<p>Sasaran yang ingin dicapai dari program/kegiatan kewirausahaan dan kerjasama adalah:</p>

				<ul>
					<li>Teraktualisasinya potensi, minat dan bakat mahasiswa dalam berbisnis dan berwirausaha;</li>
					<li>Terwujudnya jaringan kerjasama (<em>networking</em>) mahasiswa dengan alumni.</li>
				</ul>

				<p>&nbsp;</p>

				<ol start="9">
					<li><strong>Penjaminan Mutu</strong></li>
				</ol>

				<p>Sasaran yang ingin dicapai dari program/kegiatan penjaminan mutu adalah:</p>

				<ul>
					<li>Terlaksananya sistem penjaminan mutu internal (fokus: Bidang Akademik) pada Program Studi Teknik Informatika FT-UHAMKA:</li>
					<li>Terselenggaranya sistem penjaminan mutu Bidang Non-Akademik untuk mendukung tercapainya mutu Bidang Akademik.</li>
					<li>Diperolehnya peringkat terbaik dalam pelaksanaan sistem penjaminan mutu internal untuk tingkat program studi di lingkungan UHAMKA.</li>
					<li>Terdukungnya secara optimal dalam persiapan pelaksanaan penjaminan mutu eksternal (akreditasi) Program Studi Teknik Informatika UHAMKA untuk memperoleh peringkat nilai B dari BAN-PT.</li>
				</ul>

				<p>&nbsp;</p>

				<p>PROFIL LULUSAN S1 PROGRAM STUDI INFORMATIKA</p>

				<p>Sarjana Informatika lulusan Fakultas Teknik UHAMKA setelah menyelesaikan perkuliahan nya In Sya Allah mampu:</p>

				<ol>
					<li>Merencanakan, menganalisis, mendesain, dan mengimplementasi ilmu untuk bidang:</li>
					<li>Pemrosesan pengembangan sistem informasi dan rekayasa perangkat lunak beserta pemeliharaan setelah tahap pengembangannya itu.</li>
					<li>Pemrosesan instalasi konfigurasi, upgrade, adaptasi, monitoring dan perawatan database dalam suatu organisasi</li>
					<li>Dukungan, pengendalian dan pengawasan semua kegiatan yang berkaitan dengan instalasi dan pelayanan perangkat TI beserta&nbsp; jaringannya</li>
					<li>Pengembangan Ilmu pengetahuan dan teknologi informasi secara akademis yang bertanggung jawab dalam pendidikan dan pengajaran&nbsp; serta penelitian yang berkaitan dengan keilmuan komputer dan TI</li>
				</ol>

				<ol start="2">
					<li>Beriman dan bertaqwa kepada Tuhan Yang Maha Esa serta berakhlak mulia sesuai tuntunan Nabi Muhammad dan beretika dengan moral kebangsaan Indonesia dalam kehidupan bernegara&nbsp; dan bermasyarakat</li>
				</ol>

				<ol start="3">
					<li>Memiliki jiwa kepemimpinan dalam&nbsp; berkarya , meneliti dan melakukan pengabdian pada masyarakat dengan semangat kewirausahaan</li>
				</ol>

				<ol start="4">
					<li>Berinteraksi dan berkomunikasi secara efektif untuk bertukar pendapat dan bertukar pikiran dalam menerapkan ilmu pengetahuan dan teknologi di bidang informatika sehingga bermanfaat bagi masyarakat sebagai perwujudan gerakan dakwah amal ma&rsquo;ruf nahi mungkar.</li>
				</ol>

				<p>Sarjana Informatika Fakultas Teknik UHAMKA dapat&nbsp; berperan&nbsp; mengisi peluang usaha mandiri atau di dalam organisasi industrial sebagai pendukung jaringan komputer, pengelolaan basis data, perekayasa aplikasi sistem informasi&nbsp; dan dunia program hiburan digital atau berperan meningkatkan keilmuan sebagai tenaga pengajar dan peneliti di bidang&nbsp; teknologi informasi.</p>

				<p>&nbsp;</p>

				<p><strong>KURIKULUM&nbsp; PROGRAM STUDI&nbsp; INFORMATIKA</strong></p>

				<p>Kurikulum Operasional Program Studi Informatika Program Sarjana (S-1) Fakultas Teknik Universitas Muhammadiyah Prof. DR. HAMKA dikelompokkan menurut jenis mata kuliah.</p>

				<p><strong>Beban Kredit, Mata Kuliah, dan Semester</strong></p>

				<ol>
					<li>Jumlah total SKS strata satu (S1) =&nbsp; 150 sks</li>
					<li>Jumlah beban SKS Wajib =&nbsp; 147 sks</li>
					<li>Jumlah beban SKS tanpa Mata Kuliah Pilihan =&nbsp; 141 sks</li>
					<li>Jumlah SKS Mata Kuliah pilihan Wajib =&nbsp;&nbsp;&nbsp;&nbsp; 6 sks</li>
					<li>Jumlah maksimum SKS per semester =&nbsp;&nbsp; 22 sks</li>
					<li>Jumlah minimum SKS per semester =&nbsp;&nbsp; 10 sks</li>
					<li>Jumlah maksimum mata kuliah per semester =&nbsp;&nbsp; 10 MK</li>
					<li>Jumlah minimum mata kuliah per semester =&nbsp;&nbsp;&nbsp;&nbsp; 3 MK</li>
					<li>Bobot maksimum (SKS) per mata kuliah =&nbsp;&nbsp;&nbsp;&nbsp; 6 sks</li>
					<li>Bobot minimum (SKS) per mata kuliah =&nbsp;&nbsp;&nbsp;&nbsp; 1 sks</li>
					<li>Jumlah maksimum semester dalam kuliah =&nbsp;&nbsp; 14 Semester</li>
					<li>Jumlah minimum semester dalam kuliah =&nbsp;&nbsp;&nbsp;&nbsp; 7 Semester</li>
					<li>Jumlah SKS mata kuliah setiap peminatan =&nbsp;&nbsp; 18 sks</li>
					<li>Jumlah SKS mata kuliah pilihan setiap peminatan =&nbsp;&nbsp;&nbsp;&nbsp; 9 sk</li>
				</ol>

				<!-- taro sini -->
				<p><b>DAFTAR DOSEN<b></p>
					<ol>
					  <li> Sugema, S.Kom,M.Kom </li>
						<li> Atiqah Meutia Hilda, S.Kom., M.Kom.</li>
						<li> Emilia Roza, ST., M.Pd.</li>
						<li> Dwi Astuti Cahyasiwi, ST.</li>
						<li> Irfan, S.Kom </li>
						<li> Arry Avorizano, S.Kom., M.Kom</li>
					</ol>

										</div>
									</div>
								</div>
							</div>
							<!--end detail -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer>
	  <div class="verybottom">
	    <div class="container">
	      <div class="row">
	        <div class="span12">
	            <p>
	              Dibuat oleh kelompok 2 , kelas 2A :
	           
	            </p>
	          <div class="credits">
	              1. Muhammad Rifqi Maulatur Rahman (1703015041)<br>
	              2. Muhammad Khoiri Muzakki (1703015123)<br>
	              3. Rinaldi Hari Hermawan (1703015084)<br>
	              4. Muhammad Sabiil (1703015163) <br>
	              5. Nur Rahmad (1703015185)<br>
	              6. Yudha Adi H.P (1703015030)<br>
	              7. Monica Dwijayanti (1703015112)<br>
	              8. Azzizah (1703015188)
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</footer>
	<!-- end main -->
	<!-- js -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/jquery.easing.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/parallax/jquery.parallax-1.1.3.js"></script>
	<script src="assets/js/nagging-menu.js"></script>
	<script src="assets/js/jquery.nav.js"></script>
	<script src="assets/js/prettyPhoto/jquery.prettyPhoto.js"></script>
	<script src="assets/js/portfolio/jquery.quicksand.js"></script>
	<script src="assets/js/portfolio/setting.js"></script>
	<script src="assets/js/hover/jquery-hover-effect.js"></script>
	<script src="assets/js/jquery.scrollTo.min.js"></script>
	<script src="assets/js/animate.js"></script>


	<script src="assets/js/custom.js"></script>

</body>

</html>
