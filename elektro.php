<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Teknik Elektro</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" >
  <link href="assets/ico/teknik.png" rel="icon">

  <meta name="description" content="" >
  <meta name="author" content="" >
  <!-- styles -->
  <link href="assets/css/bootstrap.css" rel="stylesheet" >
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet" >
  <link href="assets/css/prettyPhoto.css" rel="stylesheet" >
  <link href="assets/css/animate.css" rel="stylesheet" >
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic|Roboto+Condensed:400,300,700" rel="stylesheet" >
  <link href="assets/css/style.css" rel="stylesheet" >
  <link href="assets/color/default.css" rel="stylesheet" >
  <link href="assets/css/custom.css" rel="stylesheet">
</head>

<body>
  <header>
    <div id="topnav" class="navbar navbar-fixed-top default">
      <div class="navbar-inner">
        <div class="container">
          <div class="logo">
            <a href="index.html"><img class="logo" src="assets/img/logo-NAV.png" alt="" ></a>
          </div>
          <div class="navigation">
            <nav>
              <ul class="nav pull-right">
                <li><a href="index.php" class="external">Beranda</a></li>
                <li><a href="index.php#profil" class="external">Profil Fakultas</a></li>
                <li><a href="index.php#prodi" class="external">Prodi</a></li>
                <li><a href="index.php#dftdosen" class="external">Daftar Dosen</a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </header>
	<section id="beranda" class="inner">
		<div class="slogan">
		  <div class="icon">
		    <i class="icon-bolt icon-10x"></i>
		  </div>
		  <h2><strong>TEKNIK ELEKTRO</strong></h2>
		</div>
	</section>
	<!-- main -->
	<section id="maincontent" class="section">
		<div class="container">
			<div class="row">
				<div class="span12">
					<div class="row">
						<div class="span12">
							<div class="accordion" id="accordion2">
								<div class="accordion-group">
									<div id="collapseOne" class="accordion-body collapse in">
										<div class="accordion-inner">
											<p><strong>PROGRAM STUDI TEKNIK ELEKTRO</strong></p>

											<p>Ketua Program Studi : Oktarina Heriyani, S.Si., MT.</p>

											<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>

											<p><strong><em>VISI</em></strong></p>

											<p><strong><em>Menjadi&nbsp;</em></strong><strong><em>p</em></strong><strong><em>rogram&nbsp;</em></strong><strong><em>s</em></strong><strong><em>tudi&nbsp;</em></strong><strong><em>utama&nbsp;</em></strong><strong><em>yang&nbsp;</em></strong><strong><em>menghasilkan lulusan&nbsp;</em></strong><strong><em>T</em></strong><strong><em>eknik&nbsp;</em></strong><strong><em>Elektro</em></strong><strong><em>&nbsp;yang&nbsp;&nbsp;</em></strong><strong><em>unggul&nbsp;</em></strong><strong><em>dalam kecerdasan</em></strong>&nbsp;<strong><em>spiritual</em></strong><strong><em>,&nbsp;</em></strong><strong><em>intelektual</em></strong><strong><em>,</em></strong><strong><em>&nbsp;emosional</em></strong><strong><em>,&nbsp;</em></strong><strong><em>dan&nbsp;</em></strong><strong><em>so</em></strong><strong><em>s</em></strong><strong><em>ial</em></strong><strong><em>&nbsp;dalam bidang&nbsp;</em></strong><strong><em>telekomunikasi</em></strong><strong><em>&nbsp;pada tahun 2020.</em></strong></p>

											<p>Pernyataan Visi Program Studi Teknik Elektro di atas mengandung maksud sebagai berikut.</p>

											<p>Program studi&nbsp;U<strong>tama</strong>&nbsp;bermakna:</p>

											<ol>
												<li>Seluruh civitas Program Studi Teknik Elektro, termasuk alumni berperilaku utama (akhlakul karimah) karena Program Studi Teknik Elektro merupakan lembaga ilmiah dan lembaga dakwah.</li>
												<li>Program Studi Teknik Elektro menjadi program studi utama (papan atas) sejajar dengan program studi ternama lainnya di DKI</li>
												<li>LulusanProgram Studi Teknik Elektro Fakultas Teknik Universitas Muhammadiyah Prof. DR. HAMKA yang berkompeten dengan spesialisasi bidang Teknik Elektro (Teknik Telekomunikasi) yang dilandasi oleh keunggulan dalam kecerdasan spiritual, intelektual, emosional, dan sosial.</li>
											</ol>

											<p><strong>Unggul</strong>&nbsp;dalam kecerdasan spiritual, intelektual, emosional, dan sosial memiliki arti sebagai berikut.</p>

											<ol>
												<li><strong>Cerdas spiritual</strong>, ditunjukkan dengan sikap dan keyakinan bahwa seluruh khasanah ilmu pengetahuan bersumber dari Allah Swt. Oleh karena itu, Program Studi Teknik Elektro UHAMKA memaknai seluruh kegiatan dan proses pendidikan yang berlangsung di kampus adalah cara anak manusia yang dengan hati nuraninya untuk terus bertaqarrub kepada Allah dalam menggapai ilmu dari sumber utamanya. Kecerdasan spiritual ini menjadi basis bagi pembentukan kecerdasan intelektual, kecerdasan emosional, dan kecerdasan sosial lulusan dan menjadi nilai unggul yang khas dari segenap civitas akademika dan lulusan Program Studi Teknik Elektro UHAMKA;</li>
												<li><strong>Cerdas intelektual</strong>, antara lain ditunjukkan dengan sifat dan sikap&nbsp;<em>smart</em>, kreatif, inovatif, objektif, tangkas, didasari oleh sikap tawadlu kepada Allah Subhanahu Wata&rsquo;ala dan mampu memerankan diri sebagai solusi bagi lingkungan;</li>
												<li><strong>Cerdas emosional</strong>, antara lain ditunjukkan dengan sifat dan sikap menyadari hakikat diri sendiri, berprinsip lebih baik &rsquo;<em>memberi</em>&rsquo; daripada &rsquo;<em>menerima</em>&rsquo; sebagai implementasi Surat Al-Maun, berempati, bersemangat untuk berprestasi, bekerja keras, tidak mudah putus asa, dan pandai bekerja sama (bersinergi);</li>
												<li><strong>Cerdas sosial</strong>, antara lain ditunjukkan dengan sifat dan sikap bermanfaat bagi lingkungan, toleran sesuai dengan petunjuk Allah pada Surat Al Kafirun, menghargai orang lain, gemar bersilahturami, menjadi bagian yang tidak terpisahkan dengan lingkungan, bertanggung jawab menjadi elemen persyarikatan Muhammadiyah.</li>
											</ol>

											<p><strong>MISI Program Studi Teknik Elektro :</strong></p>

											<ul>
												<li>Menjadikan Al-Islam dan Kemuhammadiyahan sebagai ruh dan bagian dari pendidikan, pengajaran, bersikap dan berprilaku segenap civitas akademika Program Studi Teknik Elektro.</li>
												<li>Menyelenggarakan pendidikan dan pengajaran yang bermutu.</li>
												<li>Menyelenggarakan penelitian bidang Teknik Elektro yang berkontribusi pada kemajuan ilmu pengetahuan dan teknologi dalam skala nasional dan internasional.</li>
												<li>Menyelenggarakan pengabdian dan pemberdayaan masyarakat yang berkontribusi dalam memecahkan masalah masyarakat dan peningkatan mutu pendidikan Teknik Elektro.</li>
												<li>Mengembangkan jiwa kewirausahaan dalam bidang Teknik Elektro.</li>
											</ul>

											<p><strong>TUJUAN Program Studi&nbsp;</strong><strong>Teknik Elektro&nbsp;</strong><strong>:</strong></p>

											<ul>
												<li>Mewujudkan Program Studi sebagai pusat unggulan gerakan dakwah Muhammadiyah yang menghasilkan kader persyarikatan, kader umat dan kader bangsa yang berakhlak mulia, untuk menjadi pilar dalam masyarakat utama yang diridhai oleh Allah Swt.</li>
												<li>Menghasilkan lulusan Teknik Elektro yang sukses berkarier dan menjadi profesional yang produktif.</li>
												<li>Menghasilkan penelitian bidang Teknik Elektro berskala nasional dan internasional yang bermanfaat bagi pengembangan ilmu, dunia usaha, dan masyarakat luas.</li>
												<li>Mewujudkan pengabdian dan pemberdayaan masyarakat bidang Teknik Elektro yang bermanfaat bagi persyarikatan Muhammadiyah dan masyarakat.</li>
												<li>Menghasilkan sumber daya manusia yang percaya diri, berjiwa wirausaha, serta beramal sesuai bidang ilmu Teknik Elektro untuk mewujudkan masyarakat Islam yang berkemajuan.</li>
											</ul>

											<p><strong>SASARAN&nbsp;</strong><strong>Program Studi&nbsp;</strong><strong>Teknik Elektro&nbsp;</strong><strong>:</strong></p>

											<p>Sejalan dengan visi dan misi serta tujuan Program Studi Teknik Elektro, maka ditetapkan beberapa sasaran yang mencakup bidang:</p>

											<p><strong>Al-Islam dan Kemuhammadiyahan</strong></p>

											<ol>
												<li>Peningkatan pengetahuan dan kemampuan dalam mengimplementasikan gerakan Muhammadiyah berkemajuan.</li>
												<li>Peningkatan penguasaan terhadap materi Al Islam dan Kemuhammadiyahan.</li>
												<li>Dapat mengaplikasikan gaya hidup sehat di lingkungan kampus yang sesuai dengan prinsip Al Islam dan Kemuhammadiyahan dengan menekan jumlah perokok di program studi.</li>
											</ol>

											<p><strong>Pendidikan dan Pengajaran</strong></p>

											<ol>
												<li>Pemutakhiran Kurikulum</li>
												<li>Peningkatan Kualitas Dosen</li>
												<li>Peningkatan Kualitas Proses Pembelajaran</li>
												<li>Peningkatan Suasana Akademik</li>
												<li>Peningkatan Kompetensi Mahasiswa</li>
												<li>Peningkatan Kualitas Lulusan</li>
												<li>Peningkatan Sarana dan Prasarana</li>
											</ol>

											<p><strong>Penelitian dan Pengembangan</strong></p>

											<ol>
												<li>Peningkatan penyelenggaraan penelitian Teknik Elektro</li>
												<li>Peningkatan publikasi karya ilmiah di bidang Teknik Elektro</li>
												<li>Bidang Pemberdayaan dan Pengabdian Masyarakat</li>
											</ol>

											<p>Penyelenggaraan Pemberdayaan dan Pengabdian Masyarakat mengacu bidang Teknik Elektro yang berdampak pada peningkatan:</p>

											<ol>
												<li>produktivitas</li>
												<li>kesejahteraan Masyarakat</li>
												<li>kuantitas dan kualitas kegiatan pengabdian masyarakat</li>
												<li>Pendidikan Kewirausahaan berbasis Teknik Elektro</li>
											</ol>

											<p>Pengembangan jiwa kewirausahaan dilakukan untuk peningkatan:</p>

											<ol>
												<li>minat berwirausaha bagi mahasiswa Teknik Elektro UHAMKA</li>
												<li>kuantitas dan kualitas proposal PKM-K</li>
												<li>kegiatan kewirausahaan di Program Studi Teknik Elektro</li>
											</ol>
											
											<!-- taro sini -->
											<p><b>DAFTAR DOSEN<b></p>
												<ol>
													<li> Oktarina Heriyani, S.Si., MT.</li>
													<li> Drs. Tjipta Suhaemi, M.Sc.</li>
													<li> Ir. Gunarwan Prayitno, M.Eng.</li>
													<li> M. Mujirudin, ST., MT</li>
													<li> Dra. Imas Ratna Ermawaty, M.Pd</li>
													<li> Harry Ramza, ST., MT.</li>
												</ol>

										</div>
									</div>
								</div>
							</div>
							<!--end detail -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer>
	  <div class="verybottom">
	    <div class="container">
	      <div class="row">
	        <div class="span12">
	            <p>
	              Dibuat oleh kelompok 2 , kelas 2A :
	              	            </p>
	          <div class="credits">
	              1. Muhammad Rifqi Maulatur Rahman (1703015041)<br>
	              2. Muhammad Khoiri Muzakki (1703015123)<br>
	              3. Rinaldi Hari Hermawan (1703015084)<br>
	              4. Muhammad Sabiil (1703015163) <br>
	              5. Nur Rahmad (1703015185)<br>
	              6. Yudha Adi H.P (1703015030)<br>
	              7. Monica Dwijayanti (1703015112)<br>
	              8. Azzizah (1703015188)
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</footer>
	<!-- end main -->
	<!-- js -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/jquery.easing.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/parallax/jquery.parallax-1.1.3.js"></script>
	<script src="assets/js/nagging-menu.js"></script>
	<script src="assets/js/jquery.nav.js"></script>
	<script src="assets/js/prettyPhoto/jquery.prettyPhoto.js"></script>
	<script src="assets/js/portfolio/jquery.quicksand.js"></script>
	<script src="assets/js/portfolio/setting.js"></script>
	<script src="assets/js/hover/jquery-hover-effect.js"></script>
	<script src="assets/js/jquery.scrollTo.min.js"></script>
	<script src="assets/js/animate.js"></script>


	<script src="assets/js/custom.js"></script>

</body>

</html>
